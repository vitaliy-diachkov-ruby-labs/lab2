# Base Bird class, other birds should be inherited from this class
class Bird
  attr_reader :name

  # Class constructor
  def initialize(name)
    @name = name  # The same as `self.name = name``
  end

  # Virtual method. Bird's subclasses should override this.
  def fly
    raise NotImplementedError
  end
end


class Chicken < Bird
  def fly
    "A chicken named #{@name} tries to fly. Unfortunately her wings were " \
      + "clipped."
  end
end


class Swan < Bird
  def fly
    "Swan #{@name} flies gracefully."
  end
end


class Pinguin < Bird
  def fly
    "Pinguin #{@name} can't fly."
  end
end


class Ostrich < Bird
  def fly
    "Ostrich #{@name} can't fly."
  end
end


begin
  bird = Bird.new("Abstract bird")
  bird.fly
rescue NotImplementedError
  puts "Base class methods could not be called. You should always override " \
    + "it in subclasses.\n"
end


chicken = Chicken.new("Mary")
puts chicken.fly


swan = Swan.new("Andrew")
puts swan.fly


pingiun = Pinguin.new("Frank")
puts pingiun.fly


ostrich = Ostrich.new("Bill")
puts ostrich.fly
